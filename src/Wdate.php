<?php

namespace app;

class Wdate
{
  // Параметры даты
  private $year;
  private $month;
  private $day;
  private $hour;
  private $minute;
  private $second;
  //массив для записи ошибок
  private $errors;

    /**
     * Wdate constructor.
     * @param string $date_string - строка с датой
     */
    public function __construct($date_string)
  {
     //инициализация массива ошибок
     $this->errors=[];
     //разбор строки на дату и время
     $this->builtDate($date_string);
  }

    /**
     * Метод парсит время из заданой строки и разбивает его на элементы (часы:минуты:секунды)
     * @param string $time - строка содержащая время в определённом формате
     * @return array - массив хранящий информацию о часах,минутах,секундах полученых из строки.
     */
    private function parseTime($time)
  {
      //Массив для хранения результатов разбора строки
      $result=["hour"=>false,"minute"=>false,"second"=>false];

      //Если есть разделитель времени
      if(stripos($time,":")!==false)
      {
          //Отделяем дату от врмени
          $exploded=explode(" ",trim($time));

          $time=$exploded[0];
          //Проверяем время на соответствие заданному формату xx:xx:xx
          if(preg_match("/^([01]?[0-9]|2[0-3]):([0-5][0-9](:[0-5][0-9])?)?$/",$time,$matches))
          {
              //делим время на часы,минуты,секунды
              $time_array=explode(":",$matches[0]);

              //Перебираем и записываем данные в результатирующий массив
              for ($i=0;$i<count($time_array);$i++)
               {
                   if ($i==0 && strlen($time_array[$i])>0) {
                       $result["hour"]=intval($time_array[$i]);

                   }
                   else if($i==1 && strlen($time_array[$i])>0){
                       $result["minute"]=intval($time_array[$i]);
                   }
                   else if($i==2 && strlen($time_array[$i])>0){
                       $result["second"]=intval($time_array[$i]);
                   }
               }
           // Если строка с датой не соответствует заданному формату то выводим сообщение об ошибке.
          } else $this->addError("Time is incorrect!");
      }

      return $result;
  }


    /**
     * Метод парсит дату из заданой строки и разбивает её на составляющие (день,месяц,год)
     * @param string $date - Строка содержащая дату
     * @return array - массив хранящий информацию о годах,месяцах,днях полученых из входной строки.
     */
    private function parseDate($date)
  {
      //Массив для хранения результатов разбора строки
      $result=['year'=>false,'month'=>false,'day'=>false];

      //Проверяем строку с датой на наличие даты в заданном формате (dd.mm.yyyy|mm.yyyy|yyyy)
      if(preg_match("/(\d{1,2}\.)?(\d{1,2}\.)?\d{4}/",$date)) {

          //Отделяем дату
          $exploded = explode(" ", trim($date));
          if (count($exploded) > 1) $date = $exploded[1];
          else $date = $exploded[0];
          //Делаем точную проверку исключая попадание неверных значений (например 32-й день месяца)
          if (preg_match("/^([0-9]{1,2}\.([0-9]{1,2}\.)?)?[0-9]{4}$/", $date, $matches)) {
              //Разбиваем дату на части через разделитель
              $date_array = explode(".", $matches[0]);

              $array_size = count($date_array) - 1;
              //Перебираем полученые части даты и заполняем результатирующий массив
              for ($i = $array_size; $i >= 0; $i--) {
                  if ($i == $array_size - 2) {
                      $result['day'] = intval($date_array[$i]);
                      if(($result["day"]>31||$result["day"]<1))
                          $this->addError("Days format is incorrect!");
                  }
                  else if ($i == $array_size - 1) {
                      $result['month'] = intval($date_array[$i]);
                      if(($result["month"]>12||$result["month"]<1))
                          $this->addError("Month format is incorrect!");
                  }
                  else if ($i == $array_size) {
                      $result['year'] = intval($date_array[$i]);
                      if(($result["year"]<1))
                          $this->addError("Year format is incorrect!");
                  }
              }
           //Если строка не содержит даты в нужном формате то выводим сообщение об ошибке
          } else $this->addError("Date is incorrect!");
      }

      return $result;
  }


    /**
     * Метод заполняет поля класса данными полученными при разборе входной строки.
     * Если при разборе строки возникли ошибки то метод выбрасывает исключение InvalidArgumentException с текстом ошибки.
     * @param string $date_string - строка с датой и временем
     */
    private function builtDate($date_string)
  {
      //Пробуем спарсить время из строки
      $time_array=$this->parseTime($date_string);
      //Инициализируем поля класса полученными результатами
      $this->hour=$time_array["hour"];
      $this->minute=$time_array["minute"];
      $this->second=$time_array["second"];

      //Пробуем спарсить дату из строки
      $date_array=$this->parseDate($date_string);
      //Инициализируем поля класса полученными результатами
      $this->year=$date_array["year"];
      $this->month=$date_array["month"];
      $this->day=$date_array["day"];

      //Если не удалось получить ни дату ни время то выводим сообщение об ошибке
      if($this->checkEmptyDate($time_array,$date_array) && !$this->hasErrors())
          $this->addError("Input string has incorrect format!");

      //Если есть ошибки - выбрасываем исключение InvalidArgumentException
      if($this->hasErrors()) {
          throw new \InvalidArgumentException($this->getErrors());
      }
  }


    /** Статический метод класса для сравнения двух дат.
     * @param Wdate $date1
     * @param Wdate $date2
     * @return Wdate - возвращает наибольшую из двух дат
     */
    public static function compareDates(Wdate $date1, Wdate $date2)
  {
     return $date1>$date2?$date1:$date2;
  }


    /** Метод сравнивает две даты и возвращает разницу между ними (Временной интервал).
     * @param Wdate $date - дата для сравнения
     * @return array - массив с данными о разнице между датами
     */
    public function diff(Wdate $date)
  {
      $diff_array=[];
      /**
       * Для сравнения дат получаем разницу между её элементами.
       * Чтобы в результате не было отрицательных значений - берем модуль от числа.
       */
      $diff_array["year"]=abs($this->year-$date->year);
      $diff_array["month"]=abs($this->month-$date->month);
      $diff_array["day"]=abs($this->day-$date->day);

      $diff_array["hour"]=abs($this->hour-$date->hour);
      $diff_array["minute"]=abs($this->minute-$date->minute);
      $diff_array["second"]=abs($this->second-$date->second);

      return $diff_array;
  }


    /** Метод строку с датой и временем для текущего экземпляра класса
     * @return string - строковое представление даты и времени
     */
    public function getDateString()
  {
      $date_string="";
      if($this->hour!==false) $date_string.=$this->formatNumber($this->hour);
      if($this->minute!==false) $date_string.=":".$this->formatNumber($this->minute);
      if($this->second!==false) $date_string.=":".$this->formatNumber($this->second);

      $date_string.=" ";
      if($this->day!==false) $date_string.=$this->formatNumber($this->day).".";
      if($this->month!==false) $date_string.=$this->formatNumber($this->month).".";
      if($this->year!==false) $date_string.=$this->year;

      return $date_string;

  }


    /** Метод добавляет 0 перед числами которые меньше 10
     * @param $number - число меньше 10 (например 5 или 8)
     * @return string - преобразованное число (например 05 или 08)
     */
    private function formatNumber($number)
  {
      if($number<10) $number="0".$number;

      return $number;
  }

    /** Метод добавляет новую ошибку в массив с ошибками
     * @param $error - текст ощибки
     */
    private function addError($error)
  {
      if(!empty($error) && !in_array($error,$this->errors))
      {
          $this->errors[]=$error;
      }
  }

    /** Метод проверяет наличие ошибок
     * @return bool - true если есть ошибки и false если ошибок нет.
     */
    private function hasErrors()
  {
      return !empty($this->errors);
  }

    /** Метод возвращает текстовое представление массива ошибок
     * @return string - строка с ошибками. Каждая ошибка с новой строки.
     */
  private function getErrors()
  {
      $errors='';
      if(!empty($this->errors))
      {
          foreach ($this->errors as $error)
          {
              $errors.=$error."\r\n";
          }
      }

      return $errors;
  }


    /** Метод проверяет заполненость полей даты и времени
     * @param $time_array - массив времени
     * @param $date_array - массив даты
     * @return bool - возвращает true если дата пуста, или false если дата заполнена.
     */
    private function checkEmptyDate($time_array, $date_array)
  {
      $data_array=array_merge($time_array,$date_array);

      foreach ($data_array as $item)
      {
          if($item!==false) return false;
      }

      return true;
  }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @return mixed
     */
    public function getHour()
    {
        return $this->hour;
    }

    /**
     * @return mixed
     */
    public function getMinute()
    {
        return $this->minute;
    }

    /**
     * @return mixed
     */
    public function getSecond()
    {
        return $this->second;
    }

}