<?php

namespace test;
use PHPUnit\Framework\TestCase;
use app\Wdate;

class WdateTest extends TestCase
{
    /*
     * Тест на заполненость полей даты и времени. Все поля должны быть не null.
     * */
    public function testGettersReturn()
    {
        $date=new Wdate("10:12 31.12.1993");

        $this->assertNotNull($date->getSecond(),"Seconds get empty value");
        $this->assertNotNull($date->getMinute(),"Minutes get empty value");
        $this->assertNotNull($date->getHour(),"Hours get empty value");
        $this->assertNotNull($date->getYear(),"Year get empty value");
        $this->assertNotNull($date->getMonth(),"Month get empty value");
        $this->assertNotNull($date->getDay(),"Day get empty value");
    }


    /**
     * Тест вывода текстового представления даты и времени.
     * На выходе должна быть НЕ пустая строка.
     */
   public function testHasErrorsReturnType()
    {
        $date = new Wdate("31.12.1993");

        $date_str=$date->getDateString();
        $this->assertInternalType('string', $date_str);
        $this->assertNotEmpty(trim($date_str),"Result date string is empty..");
    }

    /**
     * Тест метода сравнения двух дат (Большая/меньшая).
     * В результате выходная дата должна соостветствовать большей дате, т.е $date2.
     */
    public function testCompareDates()
    {
        $date = new Wdate("10:12:31 31.12.1993");
        $date2 = new Wdate("15:12:33 31.12.2009");

        $result=Wdate::compareDates($date,$date2);
        $this->assertEquals($date2,$result,"Incorrect result of compare");
    }

    /*Тест метода сравнения двух дат (временной интервал).
    * На выходе Должен быть не пустой массив, который содержит ключи year,month,day,hour,minute,second.
    *  @expectedException InvalidArgumentException
    * */
    public function testValidDatesDiff()
    {
        $date = new Wdate("10:12:31 31.12.1993");
        $date2 = new Wdate("31.12.1993");

        $result_array=$date->diff($date2);

        $this->assertNotEmpty( $result_array, "diff result is empty..");
        $this->assertArrayHasKey("year",$result_array,"Diff Array not contains year");
        $this->assertArrayHasKey("month",$result_array,"Diff Array not contains month");
        $this->assertArrayHasKey("day",$result_array,"Diff Array not contains day");
        $this->assertArrayHasKey("hour",$result_array,"Diff Array not contains hour");
        $this->assertArrayHasKey("minute",$result_array,"Diff Array not contains minute");
        $this->assertArrayHasKey("second",$result_array,"Diff Array not contains second");
    }

    /* Тест создания даты Wdate.
     * Создаются экземпляры Wdate со всеми допустимыми форматами. В результате не должно вываливаться никаких исключений.
     *  @expectedException InvalidArgumentException
     * */
    public function testCorrectDateFormats()
    {
       new Wdate("10:12:31 31.12.1993");

        new Wdate("10:12:31 31.12.1993");

        new Wdate("10:12 31.12.1993");

        new Wdate("10: 31.12.1993");

        new Wdate("31.12.1993");

        new Wdate("12.1993");

        new Wdate("1993");

        new Wdate("10:12");

        new Wdate("10:12:31");

    }


    /** Тест создания даты с заведомо ложными форматами строки.
     * В результате должно вываливаться исключение типа InvalidArgumentException
     *
     */
    public function testIncorrectDateFormats()
    {
        $this->expectException("InvalidArgumentException");

        new Wdate(":31 31.12.1993");

        new Wdate("10:12:99 31.12.1993");

        new Wdate("31.12.98993");

        new Wdate("10:12:31 31-12-1993");

        new Wdate("");

        new Wdate("10");
    }

}
